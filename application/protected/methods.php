<?php
/**
 * Created by PhpStorm.
 * User: Arka Banerjee
 * Date: 1/6/2017
 * Time: 11:21 PM
 */

function pr($array, $die = true) {
    echo "<pre>";
    print_r($array);
    echo "</pre>";
    if($die)
        die();
}